//
//  Category.swift
//  store_for_people
//
//  Created by Raoni de Oliveira Valadares on 3/22/19.
//  Copyright © 2019 Raoni de Oliveira Valadares. All rights reserved.
//

import Foundation

struct Category {
    let name: String
    let subcategories: [Subcategory]
    let isFeatured: Bool
    let isProductBased: Bool
}
