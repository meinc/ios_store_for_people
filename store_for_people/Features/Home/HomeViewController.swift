//
//  ViewController.swift
//  store_for_people
//
//  Created by Raoni de Oliveira Valadares on 3/21/19.
//  Copyright © 2019 Raoni de Oliveira Valadares. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    // MARK: Private UI properties
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.estimatedRowHeight = 140
        tableView.rowHeight = UITableView.automaticDimension
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addViews()
        defineAndActivateConstraints()
        registerCells()
        
        tableView.dataSource = self
        tableView.delegate = self
        
    }
}

// MARK: UITableViewDataSource

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        
        return cell
    }
}

// MARK: UITableViewDelegate

extension ViewController: UITableViewDelegate {}

extension ViewController {
    private func registerCells() {
        tableView.register(CategoryCell.self, forCellReuseIdentifier: "CategoryCell")
    }
}

// MARK: Private merhods - UI

extension ViewController {
    private func addViews() {
        view.addSubview(tableView)
    }
    
    private func defineAndActivateConstraints() {
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

//TODO: Create a use case for request

//let apiConfiguration = APIProviderConfiguration()
//let watcherServer = StoreForPeopleServerImp(apiConfiguration: apiConfiguration)
//
//CategoryNetworkImp(watcherServer: watcherServer).requestCategories { result in
//    result.analysis(ifSuccess: { categories in
//        print(categories)
//        categories
//    }, ifFailure: { error in
//        print("deu ruim")
//    })
//}
