import Foundation
import Alamofire
import enum Result.Result
import SwiftyJSON

protocol StoreForPeopleServer {
    var apiConfiguration: APIConfiguration { get }
    func execute(request: Requestable, completion: @escaping (Result<JSON, ServerError>) -> Void)
}

struct StoreForPeopleServerImp: StoreForPeopleServer {
    private let serverErrorParser: StoreForPeopleServerErrorParser
    let apiConfiguration: APIConfiguration
    
    init(apiConfiguration: APIProviderConfiguration) {
        serverErrorParser = StoreForPeopleServerErrorParser()
        self.apiConfiguration = apiConfiguration
    }
    
    func execute(request: Requestable, completion: @escaping (Result<JSON, ServerError>) -> Void) {
        Alamofire.request(
            request.path,
            method: request.method,
            parameters: request.parameters,
            encoding: request.encoding,
            headers: nil).response { dataResponse in
                if let error = self.serverErrorParser.parse(dataResponse: dataResponse) {
                    completion(.failure(error))
                }
                
                guard let data = dataResponse.data else {
                    completion(.failure(ServerError.emptyDataResponse))
                    return
                }
                
                let json = JSON(data)
                completion(.success(json))
        }
    }
}
