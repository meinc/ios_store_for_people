//
//  ParserForRequestCategoriesResponse.swift
//  store_for_people
//
//  Created by Raoni de Oliveira Valadares on 3/22/19.
//  Copyright © 2019 Raoni de Oliveira Valadares. All rights reserved.
//

import Foundation
import SwiftyJSON

final class ParserForRequestCategoriesResponse {
    func parse(json: JSON) throws -> [Category] {
        guard let categories = json.array?.compactMap(Category.init) else { throw ServerError.invalidJSON }
        
        return categories
    }
}
