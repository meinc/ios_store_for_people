//
//  Category+JSON.swift
//  store_for_people
//
//  Created by Raoni de Oliveira Valadares on 3/22/19.
//  Copyright © 2019 Raoni de Oliveira Valadares. All rights reserved.
//

import Foundation
import SwiftyJSON

extension Category {
    init?(json: JSON) {
        guard
            let name = json["name"].string,
            let subcategories: [Subcategory] = json["sub_categories"].array?.compactMap(Subcategory.init)
            else { return nil }
        
        self.name = name
        self.subcategories = subcategories
        self.isFeatured = json["is_featured"].boolValue
        self.isProductBased = json["is_product_based"].boolValue
    }
}
