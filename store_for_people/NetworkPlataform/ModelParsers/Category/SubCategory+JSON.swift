//
//  SubCategory+JSON.swift
//  store_for_people
//
//  Created by Raoni de Oliveira Valadares on 3/22/19.
//  Copyright © 2019 Raoni de Oliveira Valadares. All rights reserved.
//

import Foundation
import SwiftyJSON

extension Subcategory {
    init?(json: JSON) {
        guard let name = json["name"].string else { return nil }
        
        self.name = name
    }
}
