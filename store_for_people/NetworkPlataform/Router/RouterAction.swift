import Foundation
import Alamofire

struct RouterAction {
    enum category: RouterProtocol {
        case requestCategories
        
        var path: String {
            switch self {
            case .requestCategories: return "/api/category/new"
            }
        }
        
        var method: HTTPMethod {
            switch self {
            default:
                return .get
            }
        }
        
        var encoding: ParameterEncoding {
            switch self {
            default:
                return URLEncoding(destination: .queryString)
            }
        }
    }
    
    enum search: RouterProtocol {
        case makersWithFilters
        
        var path: String {
            switch self {
            case .makersWithFilters: return "/api/search/makers-with-filters"
            }
        }
        
        var method: HTTPMethod {
            switch self {
            default:
                return .post
            }
        }
        
        var encoding: ParameterEncoding {
            switch self {
            default:
                return URLEncoding(destination: .queryString)
            }
        }
    }
    
    enum brand: RouterProtocol {
        case configuration
        
        var path: String {
            switch self {
            case .configuration: return "/api/brand/{brand_token}/products"
            }
        }
        
        var method: HTTPMethod {
            switch self {
            default:
                return .get
            }
        }
        
        var encoding: ParameterEncoding {
            switch self {
            default:
                return URLEncoding(destination: .queryString)
            }
        }
    }
}
