import Foundation
import Result




protocol CategoryNetwork {
    func requestCategories(completion: @escaping (Result<[Category], ServerError>) -> Void)
}

final class CategoryNetworkImp: CategoryNetwork {
    private let storeForPeopleServer: StoreForPeopleServer
    
    init(watcherServer: StoreForPeopleServer) {
        self.storeForPeopleServer = watcherServer
    }
    
    public func requestCategories(completion: @escaping (Result<[Category], ServerError>) -> Void) {
        let request = RequestBuilder(
            action: RouterAction.category.requestCategories,
            configuration: storeForPeopleServer.apiConfiguration)
            .build()
        
        storeForPeopleServer.execute(request: request) { result in
            result.analysis(ifSuccess: { json in
                do {
                    let categories = try ParserForRequestCategoriesResponse().parse(json: json)
                    completion(.success(categories))
                } catch let error as ServerError {
                    completion(.failure(error))
                } catch {
                    completion(.failure(ServerError.unkown))
                }
                
            }, ifFailure: {
                completion(.failure($0))
            })
        }
    }
}
