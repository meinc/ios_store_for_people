//
//  AppDelegate.swift
//  store_for_people
//
//  Created by Raoni de Oliveira Valadares on 3/21/19.
//  Copyright © 2019 Raoni de Oliveira Valadares. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let viewController = ViewController()
        viewController.view.backgroundColor = .red
        let rootNavigationController = UINavigationController(rootViewController: viewController)
        
        window?.rootViewController = rootNavigationController
        window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
       
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {

    }

    func applicationWillTerminate(_ application: UIApplication) {
    }


}

//final class ApplicationStartupCoordinator {
//    private let window: UIWindow
//    private let rootNavigation: UINavigationController
//    private let useCasesFactory = UseCasesFactory()
//
//
//    init() {
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        rootNavigation = UINavigationController()
//    }
//
//    func start() {
//        let controller = ApplicationSplashController(useCases: useCasesFactory.configuration, completion: { _ in })
//        rootNavigation.setViewControllers([controller.viewController], animated: true)
//        window.rootViewController = rootNavigation
//        window.makeKeyAndVisible()
//    }
//}
