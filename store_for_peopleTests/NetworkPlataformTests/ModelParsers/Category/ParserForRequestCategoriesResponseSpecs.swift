//
//  ParserForRequestCategoriesResponse.swift
//  store_for_peopleTests
//
//  Created by Raoni de Oliveira Valadares on 3/22/19.
//  Copyright © 2019 Raoni de Oliveira Valadares. All rights reserved.
//

import Quick
import Nimble
import SwiftyJSON

@testable import store_for_people

final class ParserForRequestCategoriesResponseSpecs: QuickSpec {
    override func spec() {
        describe("ParserForRequestCategoriesResponse") {
            describe("Init") {
                context("when json is valid") {
                    let jsonString = CategoryJSONStringFactory().make(with: .valid)
                    let json = JSON(parseJSON: jsonString)
                    let categories = try? ParserForRequestCategoriesResponse().parse(json: json)
                    
                    
                    it("returns categories correctly") {
                        expect(categories).notTo(beNil())
                        expect(categories?.count) == 14
                    }
                }
            }
        }
    }
}

private final class CategoryJSONStringFactory {
    enum Scenario {
        case valid
    }
    
    func make(with scenario: Scenario) -> String {
        switch scenario {
        case .valid: return valid
        }
    }
    
    private var valid: String {
        return  """
        [
        {
        "name": "Top Sellers",
        "sub_categories": [],
        "is_featured": true
        },
        {
        "name": "New",
        "sub_categories": [],
        "is_featured": true
        },
        {
        "name": "All Products",
        "sub_categories": [],
        "is_featured": true
        },
        {
        "name": "Spring Ahead",
        "sub_categories": [],
        "is_featured": true
        },
        {
        "name": "Accessories",
        "sub_categories": [
        {
        "name": "Bags & Purses",
        "sub_categories": []
        },
        {
        "name": "Hats & Hair",
        "sub_categories": []
        },
        {
        "name": "Keychains",
        "sub_categories": []
        },
        {
        "name": "Kids & Babies",
        "sub_categories": []
        },
        {
        "name": "Pins & Magnets",
        "sub_categories": []
        },
        {
        "name": "Sunglasses",
        "sub_categories": []
        },
        {
        "name": "Tech",
        "sub_categories": []
        },
        {
        "name": "Wallets",
        "sub_categories": []
        },
        {
        "name": "Watches",
        "sub_categories": []
        },
        {
        "name": "Socks",
        "sub_categories": []
        },
        {
        "name": "Scarves",
        "sub_categories": []
        },
        {
        "name": "Other",
        "sub_categories": []
        }
        ],
        "is_product_based": true
        },
        {
        "name": "Home Decor",
        "sub_categories": [
        {
        "name": "Pillows & Blankets",
        "sub_categories": []
        },
        {
        "name": "Candles",
        "sub_categories": []
        },
        {
        "name": "Fragrance",
        "sub_categories": []
        },
        {
        "name": "Bedding",
        "sub_categories": []
        },
        {
        "name": "Garden",
        "sub_categories": []
        },
        {
        "name": "Lighting",
        "sub_categories": []
        },
        {
        "name": "Party",
        "sub_categories": []
        },
        {
        "name": "Organization",
        "sub_categories": []
        },
        {
        "name": "Tabletop",
        "sub_categories": []
        },
        {
        "name": "Wall Art",
        "sub_categories": []
        }
        ],
        "is_product_based": true
        },
        {
        "name": "Kids",
        "sub_categories": [
        {
        "name": "Bath",
        "sub_categories": []
        },
        {
        "name": "Home",
        "sub_categories": []
        },
        {
        "name": "Kids' Accessories",
        "sub_categories": []
        },
        {
        "name": "Kids' Stationery",
        "sub_categories": []
        },
        {
        "name": "Kids' Apparel",
        "sub_categories": []
        },
        {
        "name": "Toys",
        "sub_categories": []
        }
        ],
        "is_product_based": true
        },
        {
        "name": "Stationery",
        "sub_categories": [
        {
        "name": "Baby",
        "sub_categories": []
        },
        {
        "name": "Birthday",
        "sub_categories": []
        },
        {
        "name": "Boxed Set",
        "sub_categories": []
        },
        {
        "name": "Congratulations",
        "sub_categories": []
        },
        {
        "name": "Office Supplies",
        "sub_categories": []
        },
        {
        "name": "Encouragement",
        "sub_categories": []
        },
        {
        "name": "Everyday",
        "sub_categories": []
        },
        {
        "name": "Mother’s Day",
        "sub_categories": []
        },
        {
        "name": "Father’s Day",
        "sub_categories": []
        },
        {
        "name": "Funny",
        "sub_categories": []
        },
        {
        "name": "Gift Wrap",
        "sub_categories": []
        },
        {
        "name": "Holiday",
        "sub_categories": []
        },
        {
        "name": "Love & Friendship",
        "sub_categories": []
        },
        {
        "name": "Notebooks & Paper",
        "sub_categories": []
        },
        {
        "name": "Prints",
        "sub_categories": []
        },
        {
        "name": "Thank you",
        "sub_categories": []
        },
        {
        "name": "Valentine",
        "sub_categories": []
        },
        {
        "name": "Wedding",
        "sub_categories": []
        }
        ],
        "is_product_based": true
        },
        {
        "name": "Beauty",
        "sub_categories": [
        {
        "name": "Bath & Body",
        "sub_categories": []
        },
        {
        "name": "Hair",
        "sub_categories": []
        },
        {
        "name": "Makeup",
        "sub_categories": []
        },
        {
        "name": "Nails",
        "sub_categories": []
        },
        {
        "name": "Perfume",
        "sub_categories": []
        }
        ],
        "is_product_based": true
        },
        {
        "name": "Jewelry",
        "sub_categories": [
        {
        "name": "Bracelets",
        "sub_categories": []
        },
        {
        "name": "Earrings",
        "sub_categories": []
        },
        {
        "name": "Necklaces",
        "sub_categories": []
        },
        {
        "name": "Rings",
        "sub_categories": []
        }
        ],
        "is_product_based": true
        },
        {
        "name": "Kitchen",
        "sub_categories": [
        {
        "name": "Gourmet Food",
        "sub_categories": []
        },
        {
        "name": "Gourmet Drink",
        "sub_categories": []
        },
        {
        "name": "Mugs",
        "sub_categories": []
        },
        {
        "name": "Cookware",
        "sub_categories": []
        },
        {
        "name": "Dinnerware",
        "sub_categories": []
        },
        {
        "name": "Drinkware",
        "sub_categories": []
        },
        {
        "name": "Linens",
        "sub_categories": []
        }
        ],
        "is_product_based": true
        },
        {
        "name": "Women's Apparel",
        "sub_categories": [
        {
        "name": "Tops",
        "sub_categories": []
        },
        {
        "name": "Bottoms",
        "sub_categories": []
        },
        {
        "name": "Jackets & Outerwear",
        "sub_categories": []
        },
        {
        "name": "Intimates & Loungewear",
        "sub_categories": []
        },
        {
        "name": "Dresses",
        "sub_categories": []
        }
        ],
        "is_product_based": true
        },
        {
        "name": "Pets",
        "sub_categories": [
        {
        "name": "Pet Accessories",
        "sub_categories": []
        },
        {
        "name": "Pet Toys",
        "sub_categories": []
        }
        ],
        "is_product_based": true
        },
        {
        "name": "Men",
        "sub_categories": [
        {
        "name": "Grooming",
        "sub_categories": []
        },
        {
        "name": "Men's Accessories",
        "sub_categories": []
        },
        {
        "name": "Men's Apparel",
        "sub_categories": []
        }
        ],
        "is_product_based": true
        }
        ]
        """
    }
}
