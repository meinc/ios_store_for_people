//
//  Subcategory+JSON+Specs.swift
//  store_for_peopleTests
//
//  Created by Raoni de Oliveira Valadares on 3/22/19.
//  Copyright © 2019 Raoni de Oliveira Valadares. All rights reserved.
//

import Quick
import Nimble
import SwiftyJSON

@testable import store_for_people

final class SubcategoryJSONSpecs: QuickSpec {
    override func spec() {
        describe("Subcategory+JSON") {
            describe("Init") {
                context("when json is valid") {
                    let jsonString = SubcategoryJSONStringFactory().make(with: .valid)
                    let json = JSON(parseJSON: jsonString)
                    let subcategory = Subcategory(json: json)
                    
                    
                    it("returns subcategory correctly") {
                        expect(subcategory).notTo(beNil())
                        expect(subcategory?.name) == "Top Sellers"
                    }
                }
            }
        }
    }
}

private final class SubcategoryJSONStringFactory {
    enum Scenario {
        case valid
    }
    
    func make(with scenario: Scenario) -> String {
        switch scenario {
        case .valid: return valid
        }
    }
    
    private var valid: String {
        return  """
        {
        "name": "Top Sellers",
        }
        """
    }
}
