//
//  Category+JSON+Specs.swift
//  store_for_peopleTests
//
//  Created by Raoni de Oliveira Valadares on 3/22/19.
//  Copyright © 2019 Raoni de Oliveira Valadares. All rights reserved.
//

import Quick
import Nimble
import SwiftyJSON

@testable import store_for_people

final class CategoryJSONSpecs: QuickSpec {
    override func spec() {
        describe("Category+JSON") {
            describe("Init") {
                context("when json is valid") {
                    let jsonString = CategoryJSONStringFactory().make(with: .valid)
                    let json = JSON(parseJSON: jsonString)
                    let category = Category(json: json)
                    
                    
                    it("returns category correctly") {
                        expect(category).notTo(beNil())
                        expect(category?.name) == "Top Sellers"
                        expect(category?.subcategories.count) == 0
                        expect(category?.isFeatured) == true
                        expect(category?.isProductBased) == false
                    }
                }
                
                context("when json is valid with subcategories") {
                    let jsonString = CategoryJSONStringFactory().make(with: .validWithSubcategories)
                    let json = JSON(parseJSON: jsonString)
                    let category = Category(json: json)
                    
                    
                    it("returns correctly") {
                        expect(category).notTo(beNil())
                        expect(category?.name) == "Men"
                        expect(category?.subcategories.count) == 3
                        expect(category?.isFeatured) == false
                        expect(category?.isProductBased) == true
                    }
                }
            }
        }
    }
}

private final class CategoryJSONStringFactory {
    enum Scenario {
        case valid
        case validWithSubcategories
    }
    
    func make(with scenario: Scenario) -> String {
        switch scenario {
        case .valid: return valid
        case .validWithSubcategories: return validWithSubcategories
        }
    }
    
    private var valid: String {
        return  """
        {
        "name": "Top Sellers",
        "sub_categories": [],
        "is_featured": true
        }
        """
    }
    
    private var validWithSubcategories: String {
        return """
        {
        "name": "Men",
        "sub_categories": [
        {
        "name": "Grooming",
        "sub_categories": []
        },
        {
        "name": "Men's Accessories",
        "sub_categories": []
        },
        {
        "name": "Men's Apparel",
        "sub_categories": []
        }
        ],
        "is_product_based": true
        }
        """
    }
}
