//
//  CategoriesNetworkSpecs.swift
//  store_for_peopleTests
//
//  Created by Raoni de Oliveira Valadares on 3/22/19.
//  Copyright © 2019 Raoni de Oliveira Valadares. All rights reserved.
//

import Quick
import Nimble
import SwiftyJSON
import Result

@testable import store_for_people

final class CategoryNetworkpecs: QuickSpec {
    override func spec() {
        describe("CategoryNetwork") {
            describe("Init") {
                    let server = StoreForPeopleServerMock(apiConfiguration: APIProviderConfiguration(), requestResult: .failure(ServerError.invalidJSON))
                    let _ = CategoryNetworkImp(watcherServer: server)
                
                    it("starts without invocations") {
                        expect(server.requestInvocations) == 0
                    }
            }
            
            describe("requestCategories") {
                let server = StoreForPeopleServerMock(apiConfiguration: APIProviderConfiguration(), requestResult: .failure(ServerError.invalidJSON))
                let network = CategoryNetworkImp(watcherServer: server)
                
                context("when request has fail") {
                    network.requestCategories { result in
                        expect(result.error).toNot(beNil())
                    }
                }
                
                context("when request has fail") {
                    
                }
            }
        }
    }
}

final class StoreForPeopleServerMock: StoreForPeopleServer {
    enum RequestResult {
        case success(JSON)
        case failure(ServerError)
    }
    
    var requestInvocations = 0
    let apiConfiguration: APIConfiguration
    let requestResult: RequestResult
    
    func execute(request: Requestable, completion: @escaping (Result<JSON, ServerError>) -> Void) {
        switch requestResult {
        case .success(let json):
            completion(.success(json))
        case .failure(let error):
            completion(.failure(error))
        }
    }
    
    init(apiConfiguration: APIConfiguration, requestResult: RequestResult) {
        self.apiConfiguration = apiConfiguration
        self.requestResult = requestResult
    }
    
}
